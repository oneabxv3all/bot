var express = require('express');
var router = express.Router();
var bot = require('../bot');
var auth = require('../helpers/auth')

router.get('/', auth , function (req, res, next) {
  res.render('guilds' , { req  , bot});
});

module.exports = router;
