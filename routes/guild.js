var express = require('express');
var router = express.Router();
var auth = require('../helpers/auth')

router.get('/', auth ,function (req, res, next) {
    res.render('guild', { id : req.params.id , req : req });
});

module.exports = router;
