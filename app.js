require('dotenv').config()
const express = require('express');

// imports 
const bodyParser = require('body-parser');
const session = require('express-session');
const client = require('./bot')

client.login(process.env.TOKEN)

const app = express();

// uses
app.set('view engine', 'ejs');
app.use(express.static('public'))
app.use(bodyParser.urlencoded({extended : true}));
app.use(bodyParser.json());
app.use(session({
  secret: 'asdqcr23ryc3gqwrmimcvyw4rgv4cq8mr447cgra4cmoxcrgcqgr4cqn84r',
  resave: true,
  saveUninitialized: true,
}))


// routes
app.use('/', require('./routes/index'));
app.use('/auth', require('./routes/discord'));
app.use('/guilds', require('./routes/guilds'));
app.use('/guild/:id', require('./routes/guild'));
app.use('/divar', require('./routes/divar'));

app.listen(3000, () => {
  console.log('listening on *:3000');
});